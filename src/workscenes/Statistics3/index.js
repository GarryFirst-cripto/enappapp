import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity, StyleSheet, Dimensions, ScrollView, SafeAreaView, ImageBackground } from "react-native";
import RNSpeedometer from 'react-native-speedometer'
// import AsyncStorage from '@react-native-async-storage/async-storage'
import { ChartsIntensity } from "../Dashboard/Profile/Charts-intensity";
import { defIntensity } from '../../constants';
import arrow from '../../../assets/arrow.png'
import buttonGradient from '../../../assets/images/buttons.png';

const Statistics3 = ({ navigation, count, data }) => {
  const [countWord, setCountWord] = useState(0)

  useEffect(() => {
    const intens = Math.ceil(data[data.length - 1].countTrue / defIntensity * 100);
    setCountWord(intens);
  }, [])

  let arr = [
    { name: 'n01', key: 'k01', labelColor: '#F97272', activeBarColor: '#F97272' },
    { name: 'n02', key: 'k02', labelColor: '#F97272', activeBarColor: '#F97272' },
    { name: 'n03', key: 'k03', labelColor: '#F97272', activeBarColor: '#F97272' },
    { name: 'n04', key: 'k04', labelColor: '#F97272', activeBarColor: '#F97272' },
    { name: 'n05', key: 'k05', labelColor: '#F97272', activeBarColor: '#F97272' },
    { name: 'n06', key: 'k06', labelColor: '#FFEF60', activeBarColor: '#FFEF60' },
    { name: 'n07', key: 'k07', labelColor: '#FFEF60', activeBarColor: '#FFEF60' },
    { name: 'n08', key: 'k08', labelColor: '#FFEF60', activeBarColor: '#FFEF60' },
    { name: 'n09', key: 'k09', labelColor: '#FFEF60', activeBarColor: '#FFEF60' },
    { name: 'n10', key: 'k10', labelColor: '#FFEF60', activeBarColor: '#FFEF60' },
    { name: 'n11', key: 'k11', labelColor: '#53E773', activeBarColor: '#53E773' },
    { name: 'n12', key: 'k12', labelColor: '#53E773', activeBarColor: '#53E773' },
    { name: 'n13', key: 'k13', labelColor: '#53E773', activeBarColor: '#53E773' },
    { name: 'n14', key: 'k14', labelColor: '#53E773', activeBarColor: '#53E773' },
    { name: 'n15', key: 'k15', labelColor: '#53E773', activeBarColor: '#53E773' },
    { name: 'n16', key: 'k16', labelColor: '#53E773', activeBarColor: '#53E773' }
  ]

  return (
    <View style={styles.hostcontainer}>
      <ScrollView style={styles.scroll}>
        <View style={styles.container}>
          <Text style={styles.title}>Интенсивность</Text>
          <Text style={styles.result}>{Math.floor(countWord)}%</Text>
          <SafeAreaView style={styles.container}>
            <RNSpeedometer
              value={countWord}
              defaultValue={0}
              size={260}
              labels={arr}
              labelStyle={{ display: 'none', width: 0, fontSize: 0, color: 'transparent' }}
              innerCircleStyle={{ width: '85%', height: '85%' }}

              needleImage={arrow}
              imageWrapperStyle={{
                width: "100%", 
                height: "100%", 
                marginTop: 70
              }}
              imageStyle={{
                alignSelf: 'center',
                height: 100,
                width: 25,
                justifyContent:'center',
                marginTop:'-10%'
              }}
            />
            <View style={{ backgroundColor: 'white', width: 30, height: 18, alignSelf: 'center' }} />
          </SafeAreaView>
          <View style={styles.graph}>
            <ChartsIntensity data={data} len={7} hidden={false} koe={1} />
          </View>
          <Text style={styles.resultTextBold}>Каждый день в ударном режиме</Text>
          <Text style={styles.resultText}>
            Занимайтесь ежедневно, чтобы это вошло в привычку
          </Text>
        </View>
      </ScrollView>
      <TouchableOpacity
        onPress={() => navigation.navigate("Dashboard")}
        style={{ width: "100%" }}
      >
        <ImageBackground
          source={buttonGradient}
          style={styles.trainingButton}
        >
          <Text style={styles.buttonText}>Продолжить</Text>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
}
const windowDimensions = Dimensions.get("window");
const windowWidth = windowDimensions.width;
const windowHeight = windowDimensions.height;

const styles = StyleSheet.create({
  hostcontainer: {
    backgroundColor: "white"
  },
  container: {
    backgroundColor: "white",
    paddingHorizontal: 10,
    justifyContent: "center",
    flex: 1,
  },
  title: {
    fontSize: 30,
    color: "black",
    alignSelf: "center",
    marginTop: 50,
    fontFamily: 'Gilroy-Regular',
  },
  graph: {
    marginLeft: -25,
    marginBottom: windowHeight * 0.02
  },
  result: {
    fontSize: 60,
    color: "#2F80ED",
    alignSelf: "center",
    marginTop: 25,
    fontFamily: 'Gilroy-Regular',
  },
  inform: {
    fontSize: 18,
    marginLeft: windowWidth * 0.05,
    marginTop: 15,
    marginBottom: 20,
    fontFamily: 'Gilroy-Regular',
  },
  resultTextBold: {
    fontSize: 20,
    textAlign: "center",
    fontFamily: 'Gilroy-Regular',
    marginTop: 10,
  },
  resultText: {
    fontSize: 20,
    width: "80%",
    alignSelf: 'center',
    lineHeight: 20,
    textAlign: "center",
    marginTop: windowHeight * 0.02,
    marginBottom: windowHeight * 0.02,
    fontFamily: 'Gilroy-Regular',
  },
  trainingButton: {
    width: "100%",
    height: 70,
    alignSelf: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: windowWidth * 0.02,
    marginBottom: windowWidth * 0.1,
  },
  scroll: {
    height: windowHeight - 40 - windowWidth * 0.12
  },
  buttonText: {
    color: "white",
    alignSelf: "center",
    fontSize: 18,
    textAlign: "center",
    fontFamily: 'Gilroy-Regular',
    marginTop: -13,
    fontWeight: '700'
  },
  cancel: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '82%',
    alignItems: 'center',
    marginLeft: '10%'
  }
});

Statistics3.propTypes = {
  count: PropTypes.number,
  data: PropTypes.arrayOf(PropTypes.object)
};
Statistics3.defaultProps = {
  count: 0,
  data: []
};
const mapStateToProps = ({ stat }) => ({
  count: stat.count,
  data: stat.stat
});

export default connect(mapStateToProps)(Statistics3);